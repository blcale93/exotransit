/**
 * Created by GJ_876 on 9/6/16.
 */

/**
 * A Circle is either a planet or a star
 */

public class Circle {

    public Point center; // the center of the planet
    public double radius; // the radius of the planet
    public double velocity; // Velocity of the planet in solar-radii/sec
    public double a; // Semi-major axis in Solar Radii

    public Circle (Point center, double radius, double velocity, double a) {
        this.center = center;
        this.radius = radius;
        this.velocity = velocity;
        this.a = a;
    }

    //Shifts the center by one timestep
    public void shift (double timestep) {
        center.x += velocity * timestep * (1 - Math.pow (this.center.x / this.a, 2));
    }

    public boolean inCircle (Point point) {
        if (Math.pow ((center.x - point.x), 2)
                + Math.pow((center.y - point.y), 2) <= radius * radius) {
            return true;
        }
        return false;
    }

    //sums up the number of points within the star that aren't in the planet and returns an area in Solar Radii^2
    public double getFlux (Circle planet) {

        // The non brute force method! Still doesn't work and only God knows why :(
    	/*
        double d = planet.center.magnitude();
        double r1 = this.radius;
        double r2 = planet.radius;
        double arg1 = Math.acos (r2*r2*((d*d + r2*r2 - r1*r1)
                / (2 * d * r2)));
        double arg2 = Math.acos (r1*r1*((d*d + r1*r1 - r2*r2)
                / (2 * d * r1)));
        double arg3 = .5*Math.pow ((r1-d+r2) * (d + r2 - r1) * (d - r2 + r1) * (d + r1 + r2), .5);
        double A = arg1 + arg2 - arg3;
		*/
        
    	// Brute Force Method
        double A = 0;

        double xmin = -1.01 * radius;
        double ymin = -1.01 * radius;

        for (int i=0; i<2020; i++) {
            double x = xmin + i * .001;
            for (int j=0; j<2020; j++) {
                double y = ymin + j * .001;
                Point p = new Point(x, y); //the point in question
                if (inCircle(p) && !planet.inCircle(p)) {
                    A++; // running total
                }
            }
        }
        return A * .001 * .001; /* Accounts for the finite grid used to calculate area
                                               The box size used is not something like dx^2 */

        //return A;
    }

    public double Area () {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Center: (" + center.x + ", " + center.y + ")\n";
    }

}
