/**
 * Created by GJ_876 on 9/6/16.
 */
public class Point {

    public double x;
    public double y;

    public Point (double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double magnitude () {
        return Math.pow (x * x + y * y, .5);
    }

    @Override
    public String toString () {
        return "(" + x + ", " + y + ")";
    }
}

