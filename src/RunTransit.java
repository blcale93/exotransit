import java.io.IOException;

/**
 * Created by GJ_876 on 9/13/16.
 */
public class RunTransit {

    /* Adjust Parameters Here! */

    public static final double R_Star = 1.0; // In Solar Radii
    public static final double R_Planet = 1.305; // In Jupiter Radii
    public static final double M_Star = 1.0; // In Solar Masses
    public static final double M_Planet = 1.5; // In Jupiter Masses
    public static final double a = 0.0226; // Semi-major axis in AU
    public static final double b = .9; // Impact Parameter normalized to R_Star
    public static final double timestep = 5; // The timestep of the simulation in seconds, 5 is more than sufficient
    public static final String filename = "transit_b_.9.txt";

    public static void main (String[] args) throws IOException {
        Transit transit = new Transit (R_Star, R_Planet, M_Star, a, b, timestep);
        transit.Occult();
        System.out.println ("Done!");
    }

}
