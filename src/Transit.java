/**
 * Created by GJ_876 on 9/6/16.
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/*
Coordinate Grid is normlized to 1 solar radius
 */

public class Transit {

    public double R_Star; // In Solar Radii
    public double R_Planet; // In Solar Radii
    public double M_Star; // In kg
    public double a; // Semi-major axis in Solar Radii
    public double b; // Impact Parameter normalized to R_Star
    public double V_Planet; // Planet velocity (assumed constant) in solar radii / second
    public double timestep; // The timestep of the simulation in seconds
    public final double G = 1.982E-37; // Gravitational constant in kg-SolarRadii-sec

    public Transit (double R_Star, double R_Planet,
    		double M_Star,
    		double a, double b, double timestep) {
        this.R_Star = R_Star; this.M_Star = M_Star * 1.98855E30;
        this.R_Planet = R_Planet / 9.731;
        this.a = a * 215; this.b = b * R_Star;
        V_Planet = Math.pow (G * this.M_Star / this.a, .5);
        this.timestep = timestep;   
    }

    public void Occult () throws FileNotFoundException, IOException {

        PrintWriter writer = new PrintWriter (RunTransit.filename, "UTF-8");

        // Define a Star at the origin
        Circle star = new Circle (new Point (0, 0), RunTransit.R_Star, 0, 0);

        // Define a Planet to begin at the left of the star at a certain height
        Circle planet = new Circle (new Point (-1.75 * R_Star, b), R_Planet, V_Planet, a);
        int frameNum = 0;
        double initialXPos = planet.center.x;
        while (planet.center.x < -1 * initialXPos) {
            double brightness;
            double Normalized_flux;

            // Case 1: Star and Planet have no overlap
            if (planet.center.magnitude() - planet.radius > star.radius) {
                Normalized_flux = 1.0;
            }
            // Case 2: Planet is fully within the disc of the star
            else if (planet.center.magnitude() + planet.radius < star.radius) {
                Normalized_flux = 1 - Math.pow (planet.radius / star.radius, 2);
            }
            // Case 3: Planet and star only partially overlap => Use brute force to find area
            else {
                brightness = star.getFlux (planet); // returns the area in solar radii ^ 2
                Normalized_flux = brightness / star.Area();
            }
            writer.println (Double.toString((frameNum * timestep) / 3600) + "\t" + Double.toString (Normalized_flux));
            planet.shift (timestep);
            frameNum++;
        }
        writer.close();
    }
}
