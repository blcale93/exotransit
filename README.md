# README #

Created by: Bryson Cale
Last Updated: Oct. 4, 2016

Creates a model transit! Parameters are adjusted in RunTransit.java. Program will say "Done!" when finished running. Should take no more than a minute or two for every day transiting orbital periods.

Known errors: Some sort of rounding error when the planet transitions from partially covering the star to fully covering the star. Has something to do with the different methods used for the two cases. Even making a very tiny timestep doesn't solve the problem, but does make it unnoticeable.

The program assumes the following:
  A simple, Keplarian, circular orbit with star remaining stationary.
  If the system is at a nonzero inclination (nonzero impact parameter), the planet is assumed to move on a straight line path through the disk of the star (y coordinate is constant throughout the transit) which is a good approximation.

Output file is a columnated file of time in hours vs. rel flux. in the file "transit.txt", or whatever file you decide to call the output (can be adjusted as well in RunTransit.java).

The image file is a transit with the following parameters:

public static final double R_Star = 1.0; // In Solar Radii
public static final double R_Planet = 1.305; // In Jupiter Radii
public static final double M_Star = 1.0; // In Solar Masses
public static final double M_Planet = 1.5; // In Jupiter Masses
public static final double a = 0.0226; // Semi-major axis in AU
public static final double b = .9 (as well as .6); // Impact Parameter normalized to R_Star
public static final double timestep = 5; // The timestep of the simulation in seconds, 5 is more than sufficient